package com.isen.currency.dto;

import java.util.Objects;

public class Rate {

    private long id;
    private long idExchange;
    private String symbol;
    private float value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdExchange() {
        return idExchange;
    }

    public void setIdExchange(long idExchange) {
        this.idExchange = idExchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rate rate = (Rate) o;
        return id == rate.id && Float.compare(rate.value, value) == 0 && Objects.equals(symbol, rate.symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, symbol, value);
    }

    @Override
    public String toString() {
        return "Rate{id=" + id + ", symbol='" + symbol + '\'' + ", value=" + value + '}';
    }
}
