package com.isen.currency.dto;

import java.util.*;

public class Exchange {

    private long id;
    private Map<String, Float> rates;
    private String base;
    private Date date;

    public Exchange() {
        this.rates = new HashMap<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Map<String, Float> getRates() {
        return rates;
    }

    public void setRates(Map<String, Float> rates) {
        this.rates = rates;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exchange that = (Exchange) o;
        return Objects.equals(rates, that.rates) && Objects.equals(base, that.base) && Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rates, base, date);
    }

    @Override
    public String toString() {
        String ratesString = "{";
        for (Map.Entry<String, Float> entry : rates.entrySet()) {
            ratesString += entry.getKey() + " : " + entry.getValue() + ", ";
        }
        ratesString = ratesString.substring(0, ratesString.length() - 2);
        ratesString += "}";

        return "Currencies{" + "rates=" + ratesString + ", base='" + base + "'" + ", date=" + date + "}";
    }
}
