package com.isen.currency.api;

import com.google.gson.GsonBuilder;
import com.isen.currency.dto.Exchange;

import com.google.gson.Gson;

import com.isen.currency.db.ExchangeRate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.Locale;

public class Api {

    private static final Logger LOG = LoggerFactory.getLogger(Api.class);

    public static void insert() {
        try {
            final URI uri = URI.create("https://api.exchangeratesapi.io/latest");

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(
                    uri, HttpMethod.GET, null,
                    new ParameterizedTypeReference<String>() {
                    });

            Gson gson = new Gson();
            ExchangeRate.insertExchange(gson.fromJson(response.getBody(), Exchange.class));
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }

    public static String select() {
        List<Exchange> currencies = ExchangeRate.selectExchange();
        return new GsonBuilder().setDateFormat("yyyy-MM-dd").create().toJson(currencies);
    }

    public static float selectCurrency(String symbol) {
        float convert = ExchangeRate.selectCurrency(symbol.toUpperCase(Locale.ROOT));
        LOG.debug("Symbol : {}, convert : {}", symbol, convert);
        return convert;
    }

    public static String selectRates() {
        return ExchangeRate.selectRates();
    }

}
