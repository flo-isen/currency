package com.isen.currency.db;

import com.isen.currency.dto.Exchange;
import com.isen.currency.dto.Rate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExchangeRate {

    private static final Logger LOG = LoggerFactory.getLogger(ExchangeRate.class);

    public static void insertExchange(Exchange exchange) {
        LOG.debug("Insert rates into db....");
        try (Connection con = DB.getConnection()) {
            StringBuilder sql = new StringBuilder("INSERT INTO exchange(base, last) VALUES (?, ?)");
            PreparedStatement stmt = DB.getStatement(con, sql.toString());

            stmt.setString(1, exchange.getBase());
            Date date = new Date(exchange.getDate().getTime());
            stmt.setDate(2, date);

            stmt.executeUpdate();

            sql = new StringBuilder("SELECT id FROM exchange ORDER BY id limit 1");
            stmt = DB.getStatement(con, sql.toString());
            ResultSet set = stmt.executeQuery();

            if (set.next()) {

                long id = set.getLong(1);

                sql = new StringBuilder("INSERT INTO rate(id_exchange, symbol, value) VALUES ");
                for (Map.Entry<String, Float> entry : exchange.getRates().entrySet()) {
                    sql.append("(").append(id).append(",'").append(entry.getKey()).append("',").append(entry.getValue()).append("), ");
                }
                sql = new StringBuilder(sql.substring(0, sql.length() - 2));

                LOG.debug("Insert data successfully : {}", DB.executeUpdate(con, sql.toString()));
            } else {
                LOG.debug("Error when inserting data....");
            }
        } catch (SQLException | URISyntaxException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static List<Exchange> selectExchange() {
        List<Exchange> exchanges = new ArrayList<>();
        List<Rate> rates = new ArrayList<>();

        LOG.debug("Select data from db...");

        try (Connection con = DB.getConnection()) {
            String sql = "SELECT * FROM exchange ORDER BY base";

            ResultSet set = DB.executeRequest(con, sql);

            Exchange exchange;
            while (set.next()) {
                exchange = new Exchange();
                exchange.setId(set.getLong(1));
                exchange.setBase(set.getString(2));
                exchange.setDate(set.getDate(3));
                exchange.setRates(new HashMap<>());
                exchanges.add(exchange);
            }

            sql = "SELECT * FROM rate";
            set = DB.executeRequest(con, sql);

            Rate rateTemp;
            while (set.next()) {
                rateTemp = new Rate();
                rateTemp.setId(set.getLong(1));
                rateTemp.setIdExchange(set.getLong(2));
                rateTemp.setSymbol(set.getString(3));
                rateTemp.setValue(set.getFloat(4));
                rates.add(rateTemp);
            }
            for (Exchange exc : exchanges) {
                for (Rate rate : rates) {
                    if (exc.getId() == rate.getIdExchange())
                        exc.getRates().put(rate.getSymbol(), rate.getValue());
                }
            }
            rates.clear();
        } catch (SQLException | URISyntaxException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return exchanges;
    }

    public static float selectCurrency(String symbol) {
        float convert = 1;
        LOG.debug("Select data from db...");

        try (Connection con = DB.getConnection()) {
            String sql = "SELECT * FROM exchange ORDER BY last LIMIT 1";

            ResultSet set = DB.executeRequest(con, sql);

            Exchange exchange = new Exchange();
            if (set.next()) {
                exchange.setId(set.getLong(1));
                exchange.setBase(set.getString(2));
                exchange.setDate(set.getDate(3));
                exchange.setRates(new HashMap<>());
            }

            sql = "SELECT value FROM rate WHERE id_exchange = ? AND symbol = ?";
            PreparedStatement stmt = DB.getStatement(con, sql);
            stmt.setLong(1, exchange.getId());
            stmt.setString(2, symbol);
            set = stmt.executeQuery();
            if (set.next()) {
                convert = set.getFloat(1);
            }

        } catch (SQLException | URISyntaxException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return convert;
    }

    public static String selectRates() {
        LOG.debug("Select data from db...");

        StringBuilder rates = new StringBuilder();
        try (Connection con = DB.getConnection()) {
            String sql = "SELECT value, symbol FROM rate WHERE id_exchange = (" +
                    "SELECT id FROM exchange ORDER BY last LIMIT 1" +
                    ") ORDER BY symbol";

            ResultSet set = DB.executeRequest(con, sql);
            int nb = 1;
            while (set.next()) {

                rates.append(set.getString(2)).append(" : ").append(set.getFloat(1)).append("\t");
                if (nb == 8) {
                    rates.append("\n");
                    nb = 0;
                }
                nb++;
            }
        } catch (SQLException | URISyntaxException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return rates.toString();
    }
}
