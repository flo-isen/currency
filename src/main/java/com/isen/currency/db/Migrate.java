package com.isen.currency.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;

public class Migrate {

    private static final Logger LOG = LoggerFactory.getLogger(Migrate.class);

    public static void drop() {
        try (Connection con = DB.getConnection()) {
            LOG.debug("Dropping tables...");
            String sql = "DROP TABLE IF EXISTS rate;" +
                    "DROP TABLE IF EXISTS exchange;";

            DB.executeUpdate(con, sql);

            LOG.debug("Tables dropped successfully...");
        } catch (SQLException | URISyntaxException | ClassNotFoundException se) {
            se.printStackTrace();
        }
    }

    public static void create() {
        try (Connection con = DB.getConnection()) {
            LOG.debug("Creating tables...");
            String sql = "CREATE TABLE IF NOT EXISTS exchange(" +
                    "id int IDENTITY primary key," +
                    "base varchar(3)," +
                    "last date);";

            LOG.debug("Create table result : {}", DB.executeUpdate(con, sql));

            sql = "CREATE TABLE IF NOT EXISTS rate(" +
                    "id int IDENTITY primary key," +
                    "id_exchange int," +
                    "symbol varchar(3)," +
                    "value float," +
                    "constraint fk_rate_id_exchange foreign key(id_exchange)" +
                    "references exchange(id));";

            LOG.debug("Create table result : {}", DB.executeUpdate(con, sql));

            LOG.debug("Tables created successfully...");
        } catch (SQLException | URISyntaxException | ClassNotFoundException se) {
            se.printStackTrace();
        }
    }

}
