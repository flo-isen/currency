package com.isen.currency.db;

import java.net.URISyntaxException;
import java.sql.*;
import java.util.ResourceBundle;

public class DB {

    /**
     * Execute la request passée en paramètre.
     *
     * @param request {@link String} contient la requête SQL
     * @return {@link Integer}
     * @exception SQLException exception
     */
    public static int executeUpdate(Connection connection, String request) throws SQLException {
        return connection.createStatement().executeUpdate(request);
    }

    /**
     * Execute la request passée en paramètre.
     *
     * @param request contient la requête SQL
     * @exception SQLException exception
     */
    public static ResultSet executeRequest(Connection connection, String request) throws SQLException {
        return connection.createStatement().executeQuery(request);
    }

    /**
     * Get prepared statement
     *
     * @param connection {@link Connection}
     * @param sql {@link String}
     * @exception SQLException exception
     */
    public static PreparedStatement getStatement(Connection connection, String sql) throws SQLException {
        return connection.prepareStatement(sql);
    }

    /**
     * Methode used to get connection to database
     *
     * @return {@link Connection}
     * @exception SQLException exception
     * @exception ClassNotFoundException exception
     * @exception URISyntaxException exception
     */
    public static Connection getConnection() throws SQLException, ClassNotFoundException, URISyntaxException {
        ResourceBundle bundle = ResourceBundle.getBundle("application");
        Class.forName(bundle.getString("spring.datasource.driverClassName"));
        String user = bundle.getString("spring.datasource.username");
        String pwd = bundle.getString("spring.datasource.password");
        return DriverManager.getConnection(bundle.getString("spring.datasource.url"), user, pwd);
    }
}
