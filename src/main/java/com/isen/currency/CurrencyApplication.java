package com.isen.currency;

import com.isen.currency.api.Api;
import com.isen.currency.db.Migrate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class CurrencyApplication implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(CurrencyApplication.class);

    /**
     * Apply args passed
     *
     * @param args {@link String[]}
     */
    private static void applyArguments(String[] args) {
        boolean drop;
        boolean migrate;

        if (args.length == 0) {
            Migrate.create();
            Api.insert();
        }

        for (int i = 0; i < args.length; ++i) {
            LOG.debug("args[{}]: {}", i, args[i]);
            drop = false;
            migrate = false;

            if (args[i].contains("d") || args[i].equals("--drop")) {
                Migrate.drop();
                drop = true;
            }

            if (args[i].contains("m") || args[i].equals("--migrate")) {
                Migrate.create();
                migrate = true;
            }

            if (args[i].contains("e") || args[i].equals("--exchange")) {
                if (drop && !migrate) {
                    LOG.debug("You can't get data without migrating tables.");
                } else {
                    Api.insert();
                }
            }

            if (args[i].contains("s") || args[i].equals("--select")) {
                LOG.info(Api.select());
            }
        }
    }

    @Override
    public void run(String[] args) {
        LOG.debug("EXECUTING : command line runner");

        applyArguments(args);

        boolean quit = false;
        Scanner sc = new Scanner(System.in);
        String line;
        System.out.println("Enter a value and a currency to convert.\nLike '10.0:CAD' 'CAD:10.0' 'CAD:10.0:USD'\n" +
                "If you want to list all rates enter 'list'\nIf you want to quit enter 'quit'.");
        while (!quit) {
            System.out.print("> ");
            line = sc.nextLine();
            try {
                if (line.matches("[+-]?([0-9]*[.])?[0-9]+:[A-Z]{3}")) {
                    String[] split = line.split(":");
                    float value = Float.parseFloat(split[0]);
                    System.out.println(Api.selectCurrency(split[1]) * value + " " + split[1]);
                } else if (line.matches("[A-Z]{3}:[+-]?([0-9]*[.])?[0-9]+")) {
                    String[] split = line.split(":");
                    float value = Float.parseFloat(split[1]);
                    System.out.println(value / Api.selectCurrency(split[0]) + " EUR");
                } else if (line.matches("[A-Z]{3}:[+-]?([0-9]*[.])?[0-9]+:[A-Z]{3}")) {
                    String[] split = line.split(":");
                    float value = Float.parseFloat(split[1]);
                    float temp = value / Api.selectCurrency(split[0]);
                    System.out.println((temp * Api.selectCurrency(split[2])) + " " + split[2]);
                } else if (line.equals("quit"))
                    quit = true;
                else if (line.equals("list"))
                    System.out.println(Api.selectRates());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(CurrencyApplication.class, args);
        LOG.info("APPLICATION FINISHED");
        System.exit(0);
    }

}
