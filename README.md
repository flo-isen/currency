# Projet devises

## Installation

Linux :
```
    ./compile.sh
```
Windows :
```
    ./compile.bat
```

## Lancement 
Linux :
```
    ./compile.sh
```
Windows :
```
    ./compile.bat
```

## Utilisation

### Devise vers Euro 

```
    {DEVISE}:{VALEUR}
```
Ex: 
```
    USD:10
```

### Euro vers devise

```
    {VALEUR}:{DEVISE}
```
Ex:
```
    10:USD
```

### Devise vers devise

```
    {DEVISE}:{VALEUR}:{DEVISE}
```
Ex:
```
    CAD:10:USD
```
